let http = require("http");
let courses = [
	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}
];

http.createServer(function(request,response){

	console.log(request.url); //differentiate requests via endpoints
	console.log(request.method);//differentiate requests with http methods
	/*
		HTTP requests are differentated not only via endpoints but also with their methods.

		HTTP Methods simply tells the server what action it must take or what kind of response is needed for the request. 

		With an HTTP methods we can actually create routes with the same endpoint but with different methods. 
	*/

	if(request.url === "/" && request.method === "GET"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the GET Method");
	} else if(request.url === "/" && request.method === "POST"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the POST method");
	} else if(request.url === "/" && request.method === "PUT"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the PUT method");
	} else if(request.url === "/" && request.method === "DELETE"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the DELETE method");
	} else if(request.url === "/courses" && request.method === "GET"){
		response.writeHead(200,{'Content-Type':'application/JSON'});
		response.end(JSON.stringify(courses));
	} else if(request.url === "/courses" && request.method === "POST"){

		// Route to add a new curse, we have to receive an input from the client.
		// To be able to receive the request body or the input from the request, we have to add a way to receive that input.
		// In NodeJS, this will be done in 2 steps

		// a place holder to contain the data passed from the client.
		let requestBody = ""
		// 1st step is to receive data from the request in NodeJS is called the data step.
		// data step will read the incoming stream of data from the client and process it so we can save it in the requestBody variable.

		request.on('data',function(data){
			// console.log(data);
			requestBody += data;
		})

		// end step - this will run once or after the request data has been completely sent from the client:

		request.on('end',function(){
			requestBody = JSON.parse(requestBody);
			courses.push(requestBody);
			// console.log(courses);

			response.writeHead(200,{'Content-Type':'application/json'});
			response.end(JSON.stringify(courses));
		})
		
	}



}).listen(4000);

console.log("Server running at localhost:4000");